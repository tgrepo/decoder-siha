import numpy as np
import dstore


class DataGenerator:
    """
    This class responsible for data generation. The generated data directly stored at the database.
    It takes into consideration memory limitation. The memory threshold defined by "mem_threshold" param,
    default: 4 * 10^6.
    """
    def __init__(self, datamgr, sigma=0.1, mem_threshold=None, table_name=dstore.DataStoreTableName()):
        self._sigma = sigma
        self._datamgr = datamgr
        self._mem_threshold = mem_threshold
        if mem_threshold is None:
            self._mem_threshold = 4*10**6
        self.table_name = table_name

    def _create_table_name(self):
        """
        Create table name based on the sigma
        :return str: table name
        """
        return self.table_name.get_table_name(self._sigma)

    def gen(self, size=(3, 10)):
        """
        Generate data of size "size" and store it at the database
        :param tuple size: the shape of the required data
        """
        row_size = 4 * size[1]
        if row_size > self._mem_threshold:
            raise Exception("data row exceeds memory threshold")
        max_num_rows = size[0]
        if size[0] * row_size > self._mem_threshold:
            max_num_rows = self._mem_threshold // row_size

        self._datamgr.table_name = self._create_table_name()
        with self._datamgr.get_data_store() as store:
            # the data will be generated in "num_generators" batches,
            num_generators = int(np.ceil(size[0] / max_num_rows))
            for i in range(num_generators):
                num_rows = max_num_rows
                if i == num_generators - 1:
                    num_rows = size[0] - max_num_rows * i
                generator = _NormDistArrayGenerator(self._sigma, size=(num_rows, size[1]))
                store.put(*generator.gen())


class BatchGenerator:
    """
    BatchGenerator class reads data from the storage in batches
    """
    def __init__(self, datamgr, batch_size=100, start=0, end=None, shuffle=True):
        self._batch_size = batch_size
        self._datamgr = datamgr
        self._shuffle = shuffle
        self._start = start
        if end is None:
            end = len(datamgr)
        self._end = end
        self._data_len = end - start

    @property
    def cols(self):
        return self._datamgr.cols

    def get_num_batches(self):
        return int(np.ceil(self._data_len / self._batch_size))

    def __len__(self):
        return self.get_num_batches()

    def __iter__(self):
        num_batches = self.get_num_batches()
        store = self._datamgr.get_data_store()
        while True:
            for i in range(num_batches):
                start = self._start + i * self._batch_size
                end = start + self._batch_size
                if end > self._end:
                    end = self._end
                data, lables = store.get(start=start, end=end)
                if self._shuffle:
                    ixs = np.arange(data.shape[0])
                    np.random.shuffle(ixs)
                    data, lables = data[ixs, ...], lables[ixs]
                yield data, lables
        store.close()


class _NormDistArrayGenerator:
    """
    This class responsible for generation of vectors of ones and zeros with noise from the normal distribution.
    """
    def __init__(self, sigma, size=(100, 100)):
        self.size = size
        self.sigma = sigma

    def gen(self):
        """
        Generate labels and corresponding data with noise.
        :return np.array, np.array:
        """
        words = []
        labels = []
        for i in range(self.size[0]):
            label = np.random.randint(0, 2)
            words.append(np.random.normal(0, self.sigma, size=self.size[1]) + label)
            labels.append(label)
        return np.vstack(words), np.array(labels).reshape((-1, 1))


def create_train_test_batches_generators(datamgr, batch_size, split=0.8):
    if split <= 0 or split >= 1:
        raise Exception("split value {} out of range, should be in the range (0, 1)")
    num_train = int(len(datamgr) * split)
    return BatchGenerator(datamgr, batch_size, end=num_train), \
           BatchGenerator(datamgr, batch_size, start=num_train)


def generate_data(stds, size=(1000, 100), mem_threshold=None):
    """
    Given an array of stds, generate data-set corresponding to each std
    :param stds: array of STDs
    :param size: required data size
    :param mem_threshold: number of bytes of memory before storing to database
    """
    datamgr = dstore.DataManager()
    for std in stds:
        g = DataGenerator(datamgr=datamgr, sigma=std, mem_threshold=mem_threshold)
        g.gen(size=size)
