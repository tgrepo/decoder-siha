import numpy as np
import sqlite3
import io


def _adapt_array(arr):
    """
    transform np.array to binary string
    http://stackoverflow.com/a/31312102/190597 (SoulNibbler)
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    return sqlite3.Binary(out.read())


def _convert_array(text):
    """
    transform from string to np.array
    """
    out = io.BytesIO(text)
    out.seek(0)
    return np.load(out)


# Converts np.array to TEXT when inserting
sqlite3.register_adapter(np.ndarray, _adapt_array)

# Converts TEXT to np.array when selecting
sqlite3.register_converter("ARRAY", _convert_array)


class DataStoreTableName:
    """
    convenience class to provide conversion between the value of sigma and table name
    """

    def __init__(self, base_name='sigma'):
        self.base_name = base_name

    def get_table_name(self, sigma):
        return '{}{}'.format(self.base_name, np.round(sigma, decimals=3)).replace('.', '_')

    def get_sigma(self, table_name):
        return float(table_name.replace(self.base_name, '').replace('_', '.'))


class DataManager:
    """
    Interface class for database access
    """
    def __init__(self, dbname='decoder_data.db', table_name=None):
        self.dbname = dbname
        self.table_name = table_name

    def get_data_store(self, table_name=None):
        if table_name is None:
            table_name = self.table_name
        return DBProxy(dbname=self.dbname, table_name=table_name)

    def __len__(self):
        with self.get_data_store() as store:
            return len(store)

    @property
    def cols(self):
        """
        Get row size of the data in the database
        :return: int: number of columns
        """
        table_name = self.table_name
        if table_name is None:
            table_name = self.get_tables_list()[0]
        with self.get_data_store(table_name) as store:
            return store.get_row_len()

    def get_tables_list(self):
        """
        Get list of tables, each corresponding to a specific sigma
        :return: list str: tables list
        """
        return self.get_data_store().get_tables_list()


class DBProxy:
    """
    This class provides an abstraction of the access to the database
    """
    def __init__(self, dbname, table_name):
        self._dbname = dbname
        self._table_name = table_name
        self._con = None
        self._cur = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc, value, tb):
        self.close()

    def get_row_len(self):
        """
        Get row size of the data in the database
        :return: int: number of columns
        """
        data, _ = self.get(start=0, end=1)
        return data[0].shape[0]

    def open(self, table_name=None):
        if self._con is None:
            self._con = sqlite3.connect(self._dbname, detect_types=sqlite3.PARSE_DECLTYPES)
        if table_name is None:
            table_name = self._table_name
        if table_name is None:
            raise Exception('table name is not set')
        self.set_current_table(table_name)
        return self

    def close(self):
        if self._con is not None:
            self._con.close()
        self._con = None

    def get_tables_list(self):
        """
         Get list of tables, each corresponding to a specific sigma
         :return: list str: tables list
         """
        with sqlite3.connect(self._dbname, detect_types=sqlite3.PARSE_DECLTYPES) as con:
            query = 'SELECT name FROM sqlite_master WHERE type = "table"'
            return [table[0] for table in con.execute(query)]

    def set_current_table(self, table_name):
        query = "CREATE TABLE IF NOT EXISTS {table_name} (id INTEGER PRIMARY KEY, data ARRAY, label ARRAY)"
        self._con.execute(query.format(table_name=table_name))
        self._table_name = table_name

    def put(self, data, labels):
        self.open()
        with self._con:
            self._con.executemany("INSERT INTO {table_name}(data, label) VALUES (?,?)"
                                  .format(table_name=self._table_name), zip(data, labels))

    def _create_select_query(self, start, end):
        if end is not None and end <= start:
            raise Exception("end index ({}) <= start index ({})".format(end, start))
        start += 1
        query = 'SELECT data, label FROM {table_name}'.format(table_name=self._table_name)
        if end is None:
            query = query + ' WHERE id >= {start}'.format(start=start)
        elif start == end:
            query = query + ' WHERE id = {start}'.format(start=start)
        else:
            query = query + ' WHERE id BETWEEN {start} AND {end}'.format(start=start, end=end)
        return query

    def get(self, *, start=0, end=None):
        """
        Get data rows between [start, end)
        :param int start: first row (inclusive)
        :param int end: last row (exclusive), pass None to get all the rows to end of table
        :return np.array: (end - start) rows of data
        """
        self.open()
        words = []
        labels = []
        for row in self._con.execute(self._create_select_query(start, end)):
            words.append(row[0])
            labels.append(row[1])
        return np.vstack(words), np.array(labels)

    def __len__(self):
        """
        Get number of rows in current table
        :return int: number of rows
        """
        self.open()
        query = 'SELECT COALESCE(MAX(id), 0) FROM {table_name}'.format(table_name=self._table_name)
        return self._con.execute(query).fetchone()[0]
