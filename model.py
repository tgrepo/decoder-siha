import random
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization, Activation, LeakyReLU
from keras.optimizers import Adam, RMSprop, SGD, Adadelta, Adamax, Nadam
from keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
import dstore
import generator
import graphviz


class _OptimizerGenerator:
    """
    This class encapsulates the process of the generation of an optimizer
    """

    OPTIMIZERS = [Adam, RMSprop, SGD, Adadelta, Adamax, Nadam]
    DECAYS = [0, 1e-6, 5e-5, 5e-7]

    @staticmethod
    def gen():
        def gen_learning_rate():
            return np.round(np.random.uniform(0.05, 0.008), decimals=3)

        def gen_decay():
            return random.choice(_OptimizerGenerator.DECAYS)

        lr = gen_learning_rate()
        decay = gen_decay()

        return _Optimizer(random.choice(_OptimizerGenerator.OPTIMIZERS), lr=lr, decay=decay)


class _Optimizer:
    """
    This class holds an optimizer parameters and responsible for the optimizer creation.
    """
    def __init__(self, optimizer_class, *, lr, decay):
        self.lr = lr
        self.decay = decay
        self.optimizer_class = optimizer_class

    def __call__(self, *args, **kwargs):
        if self.optimizer_class == Adadelta:
            return self.optimizer_class()
        if self.optimizer_class == Nadam:
            return self.optimizer_class(lr=self.lr)
        return self.optimizer_class(lr=self.lr, decay=self.decay)

    def __str__(self):
        return 'optimizer\nname: {}\nlearning rate: {}\ndecay: {}'\
            .format(self.optimizer_class.__name__, self.lr, self.decay)


class _ActivationGenerator:
    """
    This class encapsulates the process of the generation of an activation function.
    """
    NAMES = ['relu', 'selu', 'leakyrelu']

    @staticmethod
    def gen():
        name = random.choice(_ActivationGenerator.NAMES)
        return _Activation(name)


class _Activation:
    """
    This class holds an activation function parameters and responsible for the underlying activation class creation.
    """

    def __init__(self, name):
        self._name = name

    def __call__(self, *args, **kwargs):
        if self._name == 'leakyrelu':
            return LeakyReLU()
        return Activation(self._name)

    def __str__(self):
        return 'activation: {}'.format(self._name)


class _Layer:
    """
    This class represents a layer, where layer is an actual dence layer, batch normalization (optional),
    dropout (optional) and its activation function.
    """

    def __init__(self, activation, size, *, input_dim=None, dropout=0.5, batch_normalization=False, index=0):
        self.activation = activation
        self.dropout = dropout
        self.size = size
        self.input_dim = input_dim
        self.batch_normalization = batch_normalization
        self._index = index

    def append_to(self, model):
        """
        append all sublayers of this layer to the model
        :param Sequantial model:
        """
        use_bias = not self.batch_normalization
        if self.input_dim is None:
            model.add(Dense(self.size, use_bias=use_bias))
        else:
            model.add(Dense(self.size, input_dim=self.input_dim, use_bias=use_bias))
        if self.batch_normalization:
            model.add(BatchNormalization())
        model.add(self.activation())
        if self.dropout < 1:
            model.add(Dropout(self.dropout))

    def append_nodes_to(self, graph):
        """
        Add the subgraph of current layer to the model's graph.
        :param Digraph graph:
        """
        layer_desc = "dence layer {}\nsize {}".format(self._index, self.size)
        first_ix = self._index * 10
        last_ix = first_ix
        graph.node(str(first_ix), layer_desc, shape='box')
        cluster_name = 'cluster {}'.format(self._index)
        layer_name = 'layer {}                 '.format(self._index)
        if self.batch_normalization:
            prev_ix = last_ix
            last_ix += 1
            graph.node(str(last_ix), 'batch normalization', shape='octagon')
            with graph.subgraph(name=cluster_name) as c:
                c.attr(label=layer_name)
                c.edge(str(prev_ix), str(last_ix))
        if self.dropout < 1:
            prev_ix = last_ix
            last_ix += 1
            graph.node(str(last_ix), 'dropout, dropout probability: {}'.format(self.dropout), shape='invtrapezium')
            with graph.subgraph(name=cluster_name) as c:
                c.attr(label=layer_name)
                c.edge(str(prev_ix), str(last_ix))

        prev_ix = last_ix
        last_ix += 1
        graph.node(str(last_ix), str(self.activation))
        with graph.subgraph(name=cluster_name) as c:
            c.attr(label=layer_name)
            c.edge(str(prev_ix), str(last_ix))

        return first_ix, last_ix


class _LayersGenerator:
    """
    This class responsible for the generation of the number of layers and the layers themself, with their different parameters.
    """
    def __init__(self, input_dim=10):
        self._input_dim = input_dim

    def gen(self):

        def gen_layer1_size(input_dim):
            return int(np.round(np.random.uniform(2/3, 1.6), decimals=2) * input_dim)

        def gen_layer_size_reduce():
            if np.random.random() > .5:
                return 1
            return np.round(np.random.uniform(.5, 1.), decimals=2)

        def gen_dropout():
            if np.random.random() > .5:
                return 1
            return np.round(np.random.uniform(.45, .9), decimals=2)

        def gen_num_layers():
            return np.random.randint(3, 7)

        def gen_add_batch_normalization():
            return np.random.normal() > 0.65

        layer1_size = gen_layer1_size(self._input_dim)
        min_layer_size = int(0.3 * self._input_dim)
        num_layers = gen_num_layers()
        activation = _ActivationGenerator.gen()
        dropout = gen_dropout()
        batch_normalization = gen_add_batch_normalization()
        layer_size_reduce = gen_layer_size_reduce()
        layers = [_Layer(activation, size=layer1_size, dropout=dropout, input_dim=self._input_dim)]
        size = layer1_size
        for i in range(1, num_layers):
            size_tmp = int(size * layer_size_reduce)
            if size_tmp > min_layer_size:
                size = size_tmp
            layers.append(_Layer(activation,
                                 size=size,
                                 dropout=dropout,
                                 input_dim=self._input_dim,
                                 batch_normalization=batch_normalization,
                                 index=i))
        return layers


class ModelGenerator:
    """
    This class responsible for the generation of models. The models parameters are randomized.
    """

    def __init__(self, layer_generator):
        self._layer_generator = layer_generator

    def gen(self, num=1):
        return [_Model(layers=self._layer_generator.gen(), optimizer=_OptimizerGenerator.gen(), index=i)
                for i in range(num)]


class _Model:
    """
    This class represents a model.
    """

    def __init__(self, *, layers, optimizer, index=0, early_stopping_monitor='acc', verbose=2):
        self._layers = layers
        self._optimizer = optimizer
        self._index = index
        self._early_stopping_monitor = early_stopping_monitor
        self.verbose = verbose

    def compile(self):
        model = Sequential()
        for layer in self._layers:
            layer.append_to(model)
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy',
                      optimizer=self._optimizer(),
                      metrics=['accuracy'])
        self._model = model


    @property
    def index(self):
        return self._index

    @property
    def input_dim(self):
        return self._input_dim

    @property
    def name(self):
        return 'model {}'.format(self._index)

    def to_graph(self):
        """
        Create graphviz graph for the visualization of the model.
        :return graphviz.Digraph:
        """
        graph = graphviz.Digraph()
        graph.attr(compound='true')
        prev = None
        for layer in self._layers:
            first, last = layer.append_nodes_to(graph)
            if prev is not None:
                graph.edge(str(prev), str(first))
            prev = last
        cur = prev + 10
        last_dense_desc = "dense layer {}\nsize 1".format(len(self._layers))
        graph.node(str(cur), last_dense_desc, shape='box')
        graph.edge(str(prev), str(cur))
        prev = cur
        cur += 10
        graph.node(str(cur), "activation sigmoid")
        graph.edge(str(prev), str(cur))
        cur += 1
        graph.node(str(cur), str(self._optimizer), shape='square')
        return graph

    def plot_history(self, postfix=''):
        """
        Plot the model's performance values.
        :param postfix: a string to appent to the label of the figure
        :return:
        """
        if self._history is None:
            raise RuntimeError('history is available after model training')
        axes_acc = plt.figure().add_subplot(211)
        axes_acc.plot(self._history.history['acc'])
        axes_acc.plot(self._history.history['val_acc'])
        axes_acc.set_title('Model {}'.format(self._index) + postfix)
        axes_acc.set_ylabel('Accuracy')
        axes_acc.legend(['Train', 'Test'], loc='lower right')
        axes_acc.get_xaxis().set_visible(False)

        axes_loss = plt.figure().add_subplot(212)
        axes_loss.plot(self._history.history['loss'])
        axes_loss.plot(self._history.history['val_loss'])
        axes_loss.set_ylabel('Loss')
        axes_loss.set_xlabel('Epoch')
        axes_loss.legend(['Train', 'Test'], loc='upper right')

    def fit(self, generator, *, epochs=10, validation_generator=None):
        """
        Train the model, optionally validate it.
        :param generator: train data generator
        :param epochs: number of epochs to train
        :param validation_generator: validation data generator
        """
        validation_steps = None
        validation_data = None
        early_stopping_monitor = self._early_stopping_monitor
        if validation_generator is not None:
            validation_steps = validation_generator.get_num_batches()
            validation_data = iter(validation_generator)
            early_stopping_monitor = 'val_' + self._early_stopping_monitor
        callbacks = [EarlyStopping(monitor=early_stopping_monitor, patience=15, verbose=self.verbose)]
        self._history = self._model.fit_generator(iter(generator),
                                                  steps_per_epoch=generator.get_num_batches(),
                                                  epochs=epochs,
                                                  validation_data=validation_data,
                                                  validation_steps=validation_steps,
                                                  callbacks=callbacks,
                                                  verbose=self.verbose)

    def evaluate(self, generator):
        """
        Evaluate the model
        :param generator: data generator
        :return: (loss, accuracy)
        """
        return self._model.evaluate_generator(iter(generator), steps=generator.get_num_batches())

    def predict(self, generator):
        """
        Make prediction using the model.
        :param generator: data generator
        :return: array of predictions
        """
        preds = self._model.predict_generator(iter(generator), steps=generator.get_num_batches())
        return np.reshape(np.round(preds).astype(np.int), -1)


class ModelPerformanceInfo:
    def __init__(self, model, accuracy, loss, sigma_str):
        self._model = model
        self.loss = loss
        self.accuracy = accuracy
        self.sigma_str = sigma_str

    @property
    def model(self):
        return self._model

    @property
    def model_name(self):
        return 'Model_' + str(self._model.index)


class DecoderBuilder:
    """
    This class does random search to find best model for each table in the database.
    """
    def __init__(self, num_tries=10, datamgr=dstore.DataManager(), table_mame_converter=dstore.DataStoreTableName()):
        self._num_tries = num_tries
        self._results = {}
        self.table_mame_converter = table_mame_converter
        self.datamgr = datamgr

    def search_best_models(self, num_models=10, batch_size=100, epochs=100, verbose=2):
        model_generator = ModelGenerator(_LayersGenerator(input_dim=self.datamgr.cols))
        models = model_generator.gen(num_models)
        for table_name in self.datamgr.get_tables_list():
            if verbose > 0:
                print('searching model for the data from the table:', table_name)
            self.datamgr.table_name = table_name
            gtrain, gtest = generator.create_train_test_batches_generators(self.datamgr, batch_size=batch_size)
            accuracy = 0
            for model in models:
                model.compile()
                model.verbose = verbose
                model.fit(gtrain, epochs=epochs, validation_generator=gtest)
                evaluation = model.evaluate(gtest)
                cur_accuracy = evaluation[1]
                if cur_accuracy > accuracy:
                    accuracy = cur_accuracy
                    self._results[table_name] = ModelPerformanceInfo(model,
                                                                     accuracy=accuracy,
                                                                     loss=evaluation[0],
                                                                     sigma_str=table_name)

    def __getitem__(self, item):
        if not isinstance(item, str):
            item = self.table_mame_converter.get_table_name(item)
        return Decoder(self._results[item])

    def get_models_imgs(self, sigmas):
        model_imgs = dict()
        for sigma in sigmas:
            decoder = getattr(self, sigma, None)
            if decoder is not None:
                fname = decoder.draw_model()
                model_imgs[decoder.model_name] = fname
        return model_imgs.items()


class Decoder:
    """
    Decoder class used for actual decoding of data, capable of describing the performance of the underlying model.
    """
    def __init__(self, model_performance_info):
        self._model_performance_info = model_performance_info

    def model2graph(self):
        return self._model_performance_info.model.to_graph()

    def plot_model_history(self, postfix=''):
        self._model_performance_info.model.plot_history(postfix)

    def decode(self, generator):
        return self._model_performance_info.model.predict(generator)

    @property
    def model_name(self):
        return self._model_performance_info.model.name

    @property
    def model_accuracy(self):
        return self._model_performance_info.accuracy

    @property
    def model_loss(self):
        return self._model_performance_info.loss

