import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm


def plot_norm_dist(stds):
    x_edge = 6
    x_axis = np.arange(-x_edge, x_edge, 0.01)
    for i in range(len(stds)):
        axes = plt.figure(i).add_subplot(111)
        plt.xticks(rotation=70)
        std = stds[i]
        axes.set_xticks([0, 0.5, 1])
        axes.plot(x_axis, norm.pdf(x_axis, 0, std))
        axes.plot(x_axis, norm.pdf(x_axis, 1, std), color='orange')
        axes.axvline(x=0, linewidth=.5)
        axes.axvline(x=0.5, color='k', linestyle='--', linewidth=.5)
        axes.axvline(x=1, color='orange', linewidth=.5)
    plt.show()


def get_stds_vector(size, min_std=0.1, max_std=2):
    return np.round(np.linspace(min_std, max_std, size, dtype=np.float), decimals=3)


def get_models_graph(stds, decoder_builder):
    """
    Extract the graph of the model corresponding for each of the stds.
    :param stds: vector of stds
    :param decoder_builder: decoder builder
    :return: set of pairs (model name, model graph)
    """
    model_imgs = set()
    for std in stds:
        decoder = decoder_builder[std]
        graph = decoder.model2graph()
        model_imgs.add((decoder.model_name, graph))
    return model_imgs

