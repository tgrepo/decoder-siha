To reproduce the environment:  
```bash
	conda env create -f environment.yml  
```
or  
```bash
	pip install -r equirements.txt  
```

To see the UML diagram of the design [follow the link](https://drive.google.com/file/d/1P8GNR1TLhwovYKp8sOVphC8jiej0nGXM/view)
  
To run the experiment:  
1) launch jupyter notebook  
```bash
	jupyter notebook  
```
2) connect your brouser to jupyter local server  
3) open experiment.ipnb notebook and run all cells  

To download evaluated experiment.ipnb [follow the link](https://drive.google.com/open?id=1RJZgpfQ2_g2MouU_zBQiTjltHAAzFF7Z)
